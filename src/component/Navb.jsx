import React, { Component } from 'react';
import '../css/sk.css';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
} from 'reactstrap';

class Navb extends Component {
  render() {
    return (
      <div>
        <Navbar className="Menu" color="light" light expand="md">
          <img
            class="icon-bar"
            height="50px"
            src="http://i63.tinypic.com/oid9xu.png"
            alt="Santen Koding"
          />
          <NavbarBrand href="/"> Santren Koding</NavbarBrand>
          <NavbarToggler />
          <Collapse navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink href="/components/">Home</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/components/">Kajian Koding</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/components/">Santren Kilat</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/components/">Mondok</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/components/">Tentang Kami</NavLink>
              </NavItem>
              <button className="btn btn-outline-success">
                Login / Signup
              </button>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}

export default Navb;
