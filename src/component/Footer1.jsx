import React, {Component} from 'react';
import '../css/sk.css';
import {} from 'reactstrap';

class Footer1 extends Component{
    render(){
        return(
            <div className="container text-center">
                <div className="row justify-content-center">
                    <div className="col-lg-5">
                        <h1>Kontak</h1><br/>
                        <i className="fa fa-phone"></i> 082225111587 <br/>
                        <i className="fa fa-envelope"></i> santrenkoding@gmail.com<br/><br/>
                        <i className="fa fa-map-marker"></i> Titik Ruang Space,<br/>
                        Jl. Ngesrep Tim. III No.67, Sumurboto, Banyumanik,<br/>
                        Kota Semarang, Jawa Tengah 50269br <br/>
                    </div>
                </div>
            </div>
        )
    }
}
export default Footer1