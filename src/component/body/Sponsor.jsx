import React, {Component} from 'react';
import '../../css/sk.css';
import {} from 'reactstrap';

class Sponsor extends Component{
    render(){
        return(
            <div className="container-fluid text-center bgc-partner py-2">
                    <h1>Sponsor & Partner</h1>
                <div className="row justify-content-md-center">
                    <div className="col col-lg-4">
                    <img className="img-partner my-3" src="http://i67.tinypic.com/2hcn70k.jpg" alt="c"/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-2">
                    <img className="img-partner2" src="https://1.bp.blogspot.com/-GZjl-D1QU6k/WgpvD8krquI/AAAAAAAAE0Y/tkrHzHHdt-4BdvmofUT7vuNtjG4ANIRPQCLcBGAs/s400/Undip.png" alt="d"/>
                    </div>
                    <div className="col-lg-2">
                    <img className="img-partner2" src="https://adiwibowo.files.wordpress.com/2012/10/logo-udinus.png?resize=370%2C358" alt="e"/>
                    </div>
                    <div className="col-lg-2">
                    <img className="img-partner2" src="https://upload.wikimedia.org/wikipedia/id/archive/6/6a/20150926142003%21Logo_unisbank.jpg" alt="f"/>
                    </div>
                    <div className="col-lg-2">
                    <img className="img-partner2" src="http://id.indonesiayp.com/img/id/c/1445918663-86-pt-java-valley-technology.png" alt="g"/>
                    </div>
                    <div className="col-lg-2">
                    <img className="img-partner2" src="https://dynamiclearningid.files.wordpress.com/2017/01/orderdilla.png?w=500" alt="h"/>
                    </div>
                    <div className="col-lg-2">
                    <img className="img-partner2" src="https://www.go-mekanik.com/assets/public/src/imgs/gomekanik/logo.png" alt="i"/>
                    </div>
                </div>
                <div className="row my-5">
                    <div className="col-lg-3">
                    <img className="img-partner3" src="https://sdk.semarangkota.go.id/web/pemkot.png" alt="j" />
                    </div>
                    <div className="col-lg-3">
                    <img className="img-partner3" src="http://www.sandec.org/images/sandec-logo.png" alt="k" />
                    </div>
                    <div className="col-lg-2">
                    <img style={{height: "100px", width: "auto"}} className="img-partner3" src="http://i64.tinypic.com/28v94w6.png" alt="h" />
                    </div>
                    <div className="col-lg-4">
                    <img style={{width: "350px"}} className="img-partner3" src="https://sdk.semarangkota.go.id/komunitas/logokomunitas/20161215114909logo-p.png" alt="x" />
                    </div>
                </div>
            </div>
        )
    }
}

export default Sponsor