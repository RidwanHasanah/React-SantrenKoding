import React, {Component} from 'react'
import '../../css/sk.css';
import {
    Container,
    Row,
    Col
  } from 'reactstrap';

  class ListKK extends Component{
      render(){
          return(
              <div className="container text-center">
                <h1 class="my-5" >Daftar Santren Kilat Koding</h1><br/>
                <div className="row">
                    <div className="col col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="card boxs">
                            <img className="card-img-top activity" src="https://lh3.googleusercontent.com/XLlyoc7OLR2dgFdnhKk-83-6_kxoTXu-6RyPcUiFc_UiSwZmuIvibNfS307Crb3IcFSbPjq1EdHNIknl-_4N5QAVpBFAZmGeC3d6ZMLwng5eAhnKBnoD1V-pcItiFukHZIq0UgA62NvO_Yg" alt="1"/>
                            <div class="card-body">
                                <h5 class="card-title">Kajian Koding #3</h5>
                                <p class="card-text">ReactJS dan Firebase Auth/Hosting</p>
                            </div>
                            <div class="card-body">
                                <a href="" class="small left-a">by Santren Koding</a>
                                <a href="" class="small right-a">Kuota Terbatas</a>
                            </div>
                        </div>
                    </div>
                    <div className="col col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="card boxs">
                            <img className="card-img-top activity" src="https://lh5.googleusercontent.com/-JajpG87BjXAxqMCAu7bs_Bb-c84GUdFtG9w0mNB_dQ1oaOnT3aRYYllYr9tHnB-rLJ8ZxpOHA=w2381" alt="2"/>
                            <div class="card-body">
                                <h5 class="card-title">Kajian Koding #2</h5>
                                <p class="card-text">ReactJS dan Firebase Auth/Hosting</p>
                            </div>
                            <div class="card-body">
                                <a href="" class="small left-a">by Santren Koding</a>
                                <a href="" class="small right-a">Kuota Terbatas</a>
                            </div>
                        </div>
                    </div>
                    <div className="col col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="card boxs">
                            <img className="card-img-top activity" src="https://lh5.googleusercontent.com/Nw8x_CE9s4N8WGggkhxit5nHdo8j03kr0daRbRTRhduOtJzwmaw3h-iR0T4iBiXrEyQqN-q_VA=w3572" alt="3"/>
                            <div class="card-body">
                                <h5 class="card-title">Kajian Koding #1</h5>
                                <p class="card-text">ReactJS dan Firebase Auth/Hosting</p>
                            </div>
                            <div class="card-body">
                                <a href="" class="small left-a">by Santren Koding</a>
                                <a href="" class="small right-a">Kuota Terbatas</a>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="btn btn-outline-success my-3">Lihat Sekuruh Kajian Koding</button>
              </div>
          )
      }
  }

  export default ListKK