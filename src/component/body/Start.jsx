import React, {Component} from 'react'
import {
    Jumbotron,
    Button
  } from 'reactstrap';

class Start extends Component{
    render(){
        return(
            <div>
                <Jumbotron className="Bgc-row1">
                <h1 className="display-3">Belajar Jadi Asyik!</h1>
                <p className="lead">
                Kegiatan Belajar Intensif Programming dan Qur'an bersama Santren Koding.
                </p>
                    <Button className="btn btn-success btn-lg">Mulai Belajar</Button>
                </Jumbotron>
            </div>
        )
    }
}

export default Start