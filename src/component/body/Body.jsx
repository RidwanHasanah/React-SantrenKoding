import React, { Component } from 'react';
import '../../css/sk.css';
import Start from './Start';
import ListKK from './ListKK';
import ListKilat from './ListKilat';
import Mondok from './Mondok';
import MondokBySK from './MondokBySK';
import Sponsor from './Sponsor';

class Body extends Component{
    render(){
        return (
          <div>
            <Start/>
            <ListKK/>
            <hr/>
            <ListKilat/>
            <hr/>
            <Mondok/>
            <hr/>
            <MondokBySK/>
            <hr/>
            <Sponsor/>
          </div>
          );
    }
}

export default Body