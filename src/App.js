import React, { Component } from 'react';
import Navb from './component/Navb';
import Body from './component/body/Body';
import Footer1 from './component/Footer1';
import Footer2 from './component/Footer2';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div>
        <Navb />
        <Body />
        <Footer1 />
        <hr/>
        <Footer2 />
      </div>
    );
  }
}

export default App;
